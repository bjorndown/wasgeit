import {
  endOfDay,
  getYear,
  isBefore,
  parse,
  parseISO,
  setHours,
  setYear,
  startOfDay,
} from 'date-fns'
import { logger } from './logging.ts'
import type { Event } from '@wasgeit/common/src/types.ts'
import { de } from 'date-fns/locale'
import { zonedTimeToUtc } from 'date-fns-tz'
import PQueue from 'p-queue'

export type RawEvent = {
  url?: string
  title?: string
  start?: string
}

export type CrawlResult = {
  key: string
  events: Event[]
  broken: { event: RawEvent; error: any }[]
  ignored: { event: RawEvent; reason: string }[]
}

export type CrawlingSummary = {
  successful: CrawlResult[]
  broken: { result?: CrawlResult; crawlerKey: string; error: string }[]
}

/**
 * Summary without complete events
 */
export type StrippedSummary = {
  successful: Omit<CrawlResult, 'events'>[]
  broken: CrawlingSummary['broken']
}

export type Crawler = {
  readonly key: string
  readonly title: string
  readonly url: string
  readonly venue: string
  readonly city: string
  crawl(): Promise<CrawlResult>
}

export const postProcess = (
  crawler: { prepareDate: (date: string) => string; dateFormat: string },
  events: RawEvent[],
): Pick<CrawlResult, 'events' | 'broken'> => {
  const today = new Date()

  // Problem: Most venues only specify day and month of their events. If a venue publishes events more
  // than a year in advance, those events will be placed in the current year.
  // Solution: Assume venues list their events chronologically. Keep track of last event's date.
  // If an event's date is older than that date the event must be from next year. Then we set the year
  // accordingly.
  let previousDate = startOfDay(new Date())

  const broken: CrawlResult['broken'] = []

  const eventsWithDate = events
    .map(event => {
      try {
        const processedEvent = processDate(
          crawler,
          event as Event,
          today,
          previousDate,
        )
        previousDate = parseISO(processedEvent.start)
        return processedEvent
      } catch (error: any) {
        broken.push({ event, error: error.message })
      }
    })
    .filter((e): e is Event => !!e)

  return { broken, events: eventsWithDate }
}

export const processDate = (
  crawler: { prepareDate: (date: string) => string; dateFormat: string },
  event: Event,
  today: Date,
  previousDate: Date | undefined,
): Event => {
  const eventDateString = crawler.prepareDate(event.start)
  const dateContainsYear =
    crawler.dateFormat.includes('yy') || crawler.dateFormat === 'ISO'
  const dateContainsTime =
    crawler.dateFormat.includes('HH') || crawler.dateFormat === 'ISO'
  try {
    const referenceTime = endOfDay(new Date())
    let eventDateLocal =
      crawler.dateFormat === 'ISO'
        ? parseISO(eventDateString)
        : parse(eventDateString, crawler.dateFormat, referenceTime, {
            locale: de,
          })

    if (!dateContainsTime) {
      // Setting the time to 8 o'clock produces more sensible calendar entries in the frontend
      // If we parse a date-only value the event's start time will be 00:00, which will create a calendar entry lasting from 00:00 to 23:59
      logger.debug('setting time', { url: event.url })
      eventDateLocal = setHours(eventDateLocal, 20)
    }

    if (
      previousDate &&
      isBefore(eventDateLocal, previousDate) &&
      !dateContainsYear
    ) {
      logger.debug('moving to next year', {
        eventDateLocal: eventDateLocal.toISOString(),
        previousDate: previousDate.toISOString(),
        url: event.url,
      })
      eventDateLocal = setYear(eventDateLocal, getYear(today) + 1)
    }

    const eventDateUtc = zonedTimeToUtc(eventDateLocal, 'Europe/Zurich')

    return {
      ...event,
      start: eventDateUtc.toISOString(),
    }
  } catch (error) {
    logger.debug(
      `error while parsing '${eventDateString}' as '${crawler.dateFormat}'`,
      {
        event,
        error,
      },
    )
    throw error
  }
}

export const runCrawlers = async (
  crawlers: Crawler[],
): Promise<CrawlingSummary> => {
  const overallResult: CrawlingSummary = { successful: [], broken: [] }
  const queue = new PQueue({ concurrency: 3 })
  const tasks = crawlers.map(crawler => {
    return async () => {
      try {
        logger.debug('crawling start', {
          crawler: crawler.title,
        })

        const result = await crawler.crawl()
        logger.debug('crawling end', {
          crawler: crawler.title,
        })
        if (result.events.length > 0) {
          overallResult.successful.push(result)
        } else {
          overallResult.broken.push({
            crawlerKey: crawler.key,
            error: 'returned no events',
            result,
          })
        }
      } catch (error: any) {
        console.error(error)
        logger.error('crawler failed', {
          crawler: crawler.key,
          error: error.message,
        })
        overallResult.broken.push({
          error: error.message,
          crawlerKey: crawler.key,
        })
      }
    }
  })
  await queue.addAll(tasks)

  return overallResult
}

const crawlers: { [key: string]: Crawler } = {}

export const register = (crawler: Crawler) => {
  if (crawler.key in crawlers) {
    throw new Error(`crawler with key '${crawler.key}' already registered`)
  }
  crawlers[crawler.key] = crawler
}

export const getCrawlers = () => Object.values(crawlers)
export const getCrawler = (name: string): Crawler => {
  if (!(name in crawlers)) {
    throw new Error(
      `crawler ${name} not registered, available are: ${Object.keys(
        crawlers,
      ).sort()}`,
    )
  }
  return crawlers[name]
}
