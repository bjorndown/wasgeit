import { getEnvVar } from './env.ts'
import { logger } from './logging.ts'
import _ from 'lodash'
import { JSDOM } from 'jsdom'
import { type CrawlResult, postProcess, type RawEvent } from './crawler.ts'

export type ContentApiPayload = {
  url: string
  viewport?: {
    width: number
    height: number
  }
  waitFor?: {
    selector: string
    visible?: boolean
    hidden?: boolean
    timeout?: number
  }
  gotoOptions?: {
    timeout?: number
    waitUntil?: 'load' | 'domcontentloaded' | 'networkidle0' | 'networkidle2'
  }
}

const getContent = async (payload: ContentApiPayload): Promise<string> => {
  const body = JSON.stringify(payload)
  logger.debug(body)
  const response = await fetch(
    `https://chrome.browserless.io/content?token=${getEnvVar('BROWSERLESS_TOKEN')}`,
    { body, method: 'POST', headers: { 'Content-Type': 'application/json' } },
  )

  const text = await response.text()
  if (response.ok) {
    return text
  }
  throw new Error(
    `Browserless API returned an error: ${response.status} ${response.statusText}: ${text}`,
  )
}

export const getElements = (
  document: Document | Element,
  selector: string,
): Element[] => {
  return Array.from(document.querySelectorAll(selector).values())
}

export const getText = (element: Element, selector: string): string => {
  return (
    element
      .querySelector(selector)
      ?.textContent?.trim()
      .replaceAll(/[\n\t]+/g, ' ')
      .replaceAll(/ {2,}/g, ' ')
      .replaceAll('\u200B', '') ?? ''
  )
}

export const getAttributeText = (
  element: Element,
  selector: string,
  attributeName: string,
): string => {
  return (
    element
      .querySelector(selector)
      ?.getAttribute(attributeName)
      ?.trim()
      .replaceAll(/[\n\t]+/g, ' ')
      .replaceAll(/ {2,}/g, ' ')
      .replaceAll('\u200B', '') ?? ''
  )
}

export abstract class JSDOMBasedCrawler {
  abstract readonly key: string
  abstract readonly title: string
  abstract readonly url: string
  abstract readonly city: string
  readonly dateFormat: 'ISO' | string = 'ISO'
  readonly waitMsBeforeCrawl?: number

  async crawl(): Promise<CrawlResult> {
    const ignored: CrawlResult['ignored'] = []

    const rawEvents = await this.getRawEvents()

    const completeEvents = rawEvents.filter((event: RawEvent) => {
      const isIncomplete =
        _.isEmpty(event.start) || _.isEmpty(event.title) || _.isEmpty(event.url)

      if (isIncomplete) {
        ignored.push({ event, reason: 'incomplete' })
        return false
      }
      return true
    })

    const { events, broken } = postProcess(this, completeEvents)

    const eventsWithVenue = events.map(event => ({
      ...event,
      venue: this.venue,
    }))

    return { key: this.key, events: eventsWithVenue, broken, ignored }
  }

  async getDocument(): Promise<Document> {
    const html = await getContent(this.getContentApiPayload())
    const dom = new JSDOM(html)
    return dom.window.document
  }

  async getRawEvents(): Promise<RawEvent[]> {
    const document = await this.getDocument()

    const elements = this.getEventElements(document)

    logger.debug(`found ${elements.length} events`)

    return elements.map(element => {
      const title = this.getTitle(element)
      const start = this.getStart(element)
      const url = this.getUrl(element)
      return { title, start, url }
    })
  }

  prepareDate(date: string) {
    return date
  }

  abstract getEventElements(page: Document): Element[]

  abstract getTitle(element: Element): string | undefined

  abstract getStart(element: Element): string | undefined

  abstract getUrl(element: Element): string | undefined

  protected getContentApiPayload(): ContentApiPayload {
    return { url: this.url }
  }
  get venue(): string {
    return `${this.title}, ${this.city}`
  }

  onLoad() {}
}
