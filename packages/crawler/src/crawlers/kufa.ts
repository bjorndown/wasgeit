import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Kufa extends JSDOMBasedCrawler {
  key = 'kufa'
  title = 'KUFA'
  url = `https://www.kufa.ch`
  city = 'Lyss'
  dateFormat = 'dd.MM.yyyy'

  override prepareDate(date: string) {
    return date.split(' ')?.[1]
  }

  override getEventElements(page: Document) {
    return getElements(page, '.post-listing-entry-event article a')
  }

  override getTitle(element: Element) {
    return getText(element, '.title')
  }

  override getStart(element: Element) {
    return getText(element, '.info')
  }

  override getUrl(element: Element) {
    return element.getAttribute('href') ?? ''
  }
}

register(new Kufa())
