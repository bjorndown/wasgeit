import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Roessli extends JSDOMBasedCrawler {
  key = 'roessli'
  title = 'Rössli'
  url = 'https://www.souslepont-roessli.ch/events/'
  city = 'Bern'
  dateFormat = 'dd. MMMM yyyy'

  prepareDate(date: string) {
    return date.slice(4, 16).replace('Mrz', 'Mär').replace('MRZ', 'Mär')
  }

  getEventElements(page: Document) {
    return getElements(page, 'div.event > a')
  }

  getStart(element: Element) {
    return getText(element, 'time.event-date')
  }

  getTitle(element: Element) {
    return getText(element, 'h2')
  }

  getUrl(element: Element) {
    return element.getAttribute('href') ?? undefined
  }
}

register(new Roessli())
