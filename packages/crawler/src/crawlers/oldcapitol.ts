import { type CrawlResult, register } from '../lib/crawler.ts'
import type { Event, ISODate } from '@wasgeit/common/src/types.ts'

type OCFlowResponse = Record<
  string,
  {
    headline: string
    subtitle: string
    datum: ISODate
    dooropens: string
    slurg: string
  }
>

class OldCapitol {
  BASE_URL = 'https://oldcapitol.ch'
  key = 'oldcapitol'
  title = 'Old Capitol'
  url = new URL('/events', this.BASE_URL).toString()
  city = 'Langenthal'
  dateFormat = 'dd.MM.'
  waitMsBeforeCrawl = 1_000

  async crawl(): Promise<CrawlResult> {
    const response = await fetch(
      'https://oldcapitol.io/wp-content/plugins/oc-flow/api/events/index.php',
    )
    if (!response.ok) {
      throw new Error('')
    }
    const ocResponse: OCFlowResponse = await response.json()
    const events: Event[] = Object.values(ocResponse).map(event => ({
      title: `${event.headline}: ${event.subtitle}`,
      start: `${event.datum} ${event.dooropens}`,
      venue: this.venue,
      url: new URL(event.slurg, this.BASE_URL).toString(),
    }))
    return { key: this.key, events, broken: [], ignored: [] }
  }

  get venue(): string {
    return `${this.title}, ${this.city}`
  }
}

register(new OldCapitol())
