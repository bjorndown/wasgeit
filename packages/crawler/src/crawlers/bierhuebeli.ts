import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Bierhuebeli extends JSDOMBasedCrawler {
  key = 'bierhuebeli'
  title = 'Bierhübeli'
  url = 'https://bierhuebeli.ch'
  city = 'Bern'
  dateFormat = 'dd.MM.yy'

  override prepareDate(date: string) {
    return date.slice(6)
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.datumlink')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.eventdatum')
  }

  override getTitle(element: Element): string | undefined {
    const title = getText(element, '.eventtitel')
    const byline = getText(element, '.byline')
    return byline ? `${title} - ${byline}` : title
  }

  override getUrl(element: Element): string | undefined {
    return element.getAttribute('href') ?? undefined
  }
}

register(new Bierhuebeli())
