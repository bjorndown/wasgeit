import { register } from '../lib/crawler.ts'
import type { CrawlResult } from '../lib/crawler.ts'
import type { Event, ISODateTime } from '@wasgeit/common/src/types.ts'

type NodeHiveEvent = {
  attributes: {
    field_title: string
    field_event_date: {
      value: ISODateTime
      endValue: ISODateTime
    }
    path: {
      alias: string
    }
  }
}

type NodeHiveApiResponse = {
  data: NodeHiveEvent[]
}

class Dynamo {
  key = 'dynamo'
  BASE_URL = 'https://www.dynamo.ch'
  title = 'Dynamo'
  city = 'Zürich'
  url = new URL('/veranstaltungen', this.BASE_URL).toString()
  dateFormat = 'ISO'

  get venue(): string {
    return `${this.title}, ${this.city}`
  }

  async crawl(): Promise<CrawlResult> {
    const response = await fetch(
      'https://dynamo.nodehive.app/jsonapi/node/event',
    )
    if (!response.ok) {
      const text = await response.text()
      throw new Error(`Dynamo broken ${text}`)
    }
    const nodeHiveResponse: NodeHiveApiResponse = await response.json()
    const events: Event[] = nodeHiveResponse.data.map(event => {
      return {
        title: event.attributes.field_title,
        start: event.attributes.field_event_date.value,
        url: new URL(event.attributes.path.alias, this.BASE_URL).toString(),
        venue: this.venue,
      }
    })
    return { broken: [], events, ignored: [], key: this.key }
  }
}

register(new Dynamo())
