import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Cafete extends JSDOMBasedCrawler {
  key = 'cafete'
  title = 'Cafete'
  url = 'https://cafete.ch/'
  city = 'Bern'
  dateFormat = "EE dd. MMMM yyyy HH'h'mm"

  override prepareDate(date: string) {
    return date.replace(' — Doors:', '')
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.event')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.date')
  }

  override getTitle(element: Element): string | undefined {
    const title = getText(element, '.title')
    const acts = getText(element, '.acts')
    return `${title} ${acts}`
  }

  override getUrl(): string | undefined {
    return this.url
  }
}

register(new Cafete())
