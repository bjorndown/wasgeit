import { register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

class Sedel extends JSDOMBasedCrawler {
  key = 'sedel'
  BASE_URL = 'https://sedel.ch'
  title = 'Sedel'
  url = new URL('/club', this.BASE_URL).toString()
  city = 'Emmenbrücke'

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.month-list ul li')
  }

  override getStart(element: Element): string | undefined {
    return getAttributeText(element, 'time', 'datetime')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.views-field-title')
  }

  override getUrl(element: Element): string | undefined {
    const href = getAttributeText(element, 'a', 'href')
    return new URL(href ?? '', this.BASE_URL).toString()
  }
}

register(new Sedel())
