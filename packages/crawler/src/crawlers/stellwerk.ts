import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Stellwerk extends JSDOMBasedCrawler {
  key = 'stellwerk'
  BASE_URL = 'https://www.stellwerk.be'
  title = 'Stellwerk'
  url = new URL('/', this.BASE_URL).toString()
  city = 'Bern'
  dateFormat = 'EE dd.MM'
  waitMsBeforeCrawl = 1_000

  override getEventElements(page: Document): Element[] {
    return getElements(page, 'article.post > a')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.post__date')
  }

  override getTitle(element: Element): string | undefined {
    const title = getText(element, 'h2')
    const subTitle = getText(element, 'h3')
    return subTitle ? `${title}, ${subTitle}` : title
  }

  override getUrl(element: Element): string | undefined {
    const href = element.getAttribute('href') ?? ''
    return new URL(href, this.BASE_URL).toString()
  }
}

register(new Stellwerk())
