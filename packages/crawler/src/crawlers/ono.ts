import { register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

// The wordpress plugin (probably https://plugins.trac.wordpress.org/browser/eventon-lite?order=name)
// outputs dates where the month or day are not zero-padded, which is not a valid ISO 8601 date
export const fixUnpaddedMonthAndDay = (date: string): string => {
  const [[year], [month], [day, time]] = date
    .split('-')
    .map(token => token.split('T'))
  return `${year}-${month.padStart(2, '0')}-${day.padStart(2, '0')}T${time}`
}

class Ono extends JSDOMBasedCrawler {
  key = 'ono'
  title = 'Ono'
  url = 'https://www.onobern.ch/homepage/'
  city = 'Bern'

  prepareDate(date: string): string {
    if (date.length !== 22) {
      return fixUnpaddedMonthAndDay(date)
    }
    return date
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.eventon_list_event.scheduled.event')
  }

  override getStart(element: Element): string | undefined {
    return getAttributeText(element, 'meta[itemprop=startDate]', 'content')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.evcal_event_title')
  }

  override getUrl(element: Element): string | undefined {
    return getAttributeText(element, 'a[itemprop=url]', 'href')
  }
}

register(new Ono())
