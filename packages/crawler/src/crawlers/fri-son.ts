import { register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

class FriSon extends JSDOMBasedCrawler {
  key = 'fri-son'
  BASE_URL = 'https://fri-son.ch'
  title = 'Fri-Son'
  url = new URL('/de/program', this.BASE_URL).toString()
  city = 'Fribourg'

  override getEventElements(page: Document): Element[] {
    return getElements(page, 'article.node--type-event')
  }

  override getStart(element: Element): string | undefined {
    return getAttributeText(element, 'time', 'datetime')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.content-wrapper')
  }

  override getUrl(element: Element): string | undefined {
    const path = getAttributeText(element, 'a', 'href')
    return new URL(path, this.BASE_URL).toString()
  }
}

register(new FriSon())
