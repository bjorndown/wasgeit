import { type RawEvent, register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

class Lacapella extends JSDOMBasedCrawler {
  key = 'lacapella'
  BASE_URL = 'https://www.la-cappella.ch'
  title = 'La Cappella'
  url = new URL('de/spielplan-4.html', this.BASE_URL).toString()
  city = 'Bern'
  dateFormat = "dd. MMMM yyyy HH:mm 'Uhr'"

  override async getRawEvents(): Promise<RawEvent[]> {
    const document = await this.getDocument()
    const monthGroups = this.getEventElements(document)

    const eventsPerMonth = monthGroups.map(monthGroup => {
      const monthYear = getText(monthGroup, 'h2')
      const eventElements = getElements(monthGroup, '.lc-event')

      return eventElements.map(element => {
        const date = getText(element, '.lc-event__date')
        const time = getText(element, '.lc-event__time')
        const title = this.getTitle(element)
        const url = this.getUrl(element)
        return { start: `${date} ${monthYear} ${time}`, title, url }
      })
    })

    return eventsPerMonth.flat()
  }

  override getEventElements(page: Document) {
    return getElements(page, '.eventlist__group')
  }

  override getStart() {
    return ''
  }

  override getTitle(element: Element) {
    return getText(element, '.lc-event__text')
  }

  override getUrl(element: Element) {
    const href = getAttributeText(element, '.lc-event__text', 'href')
    return new URL(href, this.BASE_URL).toString()
  }
}

register(new Lacapella())
