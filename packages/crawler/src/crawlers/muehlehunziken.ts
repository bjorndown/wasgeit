import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Muehlehunziken extends JSDOMBasedCrawler {
  key = 'muehlehunziken'
  title = 'Mühle Hunziken'
  url = 'https://muehlehunziken.ch/programm'
  city = 'Rubigen'
  dateFormat = 'd.M.'

  override getEventElements(page: Document) {
    return getElements(page, 'main ul > li > a.customLink')
  }

  override getTitle(element: Element) {
    return getText(element, 'div > div:nth-child(2)')
  }

  override getStart(element: Element) {
    return getText(element, 'div > div:nth-child(1)')
  }

  override getUrl(element: Element) {
    return element.getAttribute('href') ?? ''
  }

  override prepareDate(date: string) {
    return date.slice(3)
  }
}

register(new Muehlehunziken())
