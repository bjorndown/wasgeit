import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Heiterefahne extends JSDOMBasedCrawler {
  key = 'heiterefahne'
  BASE_URL = 'https://www.dieheiterefahne.ch'
  title = 'Heitere Fahne'
  url = new URL('/events', this.BASE_URL).toString()
  city = 'Bern'
  dateFormat = 'dd.MM.yyyy'
  waitMsBeforeCrawl = 400

  override prepareDate(date: string) {
    return date.split(' ')[1].slice()
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.vb-content a')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.events__list-item-date')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.events__list-item-title')
  }

  override getUrl(element: Element): string | undefined {
    const href = element.getAttribute('href') ?? ''
    return new URL(href, this.BASE_URL).toString()
  }
}

register(new Heiterefahne())
