import { register } from '../lib/crawler.ts'
import {
  type ContentApiPayload,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

class Marta extends JSDOMBasedCrawler {
  key = 'marta'
  title = 'Marta'
  url = 'https://www.cafemarta.ch'
  city = 'Bern'
  dateFormat = 'MMMd'

  override getEventElements(page: Document) {
    return getElements(page, '.eapp-events-calendar-grid-item-component')
  }

  override getStart(element: Element) {
    return getText(element, '.eapp-events-calendar-grid-item-date')
  }

  override getTitle(element: Element) {
    return getText(element, '.eapp-events-calendar-grid-item-name')
  }

  override getUrl() {
    return this.url
  }

  override prepareDate(date: string): string {
    return date
      .replace('MAY', 'Mai')
      .replace('Oct', 'Okt')
      .replace('Dec', 'Dez')
  }

  override getContentApiPayload(): ContentApiPayload {
    return { url: this.url, viewport: { width: 400, height: 2000 } }
  }
}

register(new Marta())
