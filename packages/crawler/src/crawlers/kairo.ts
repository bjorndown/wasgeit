import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Kairo extends JSDOMBasedCrawler {
  key = 'kairo'
  title = 'Kairo'
  url = 'https://www.cafe-kairo.ch'
  city = 'Bern'
  dateFormat = 'dd.MM.yyyy'

  override prepareDate(date: string) {
    return date.slice(4, 14)
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, 'article[id]')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, 'p')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, 'h2')
  }

  override getUrl(element: Element): string | undefined {
    return `${this.url}/#${element.getAttribute('id')}`
  }
}

register(new Kairo())
