import { register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

class Gaskessel extends JSDOMBasedCrawler {
  key = 'gaskessel'
  BASE_URL = 'https://gaskessel.ch'
  title = 'Gaskessel'
  url = new URL('/programm/', this.BASE_URL).toString()
  city = 'Bern'
  dateFormat = 'dd.MM.yy'

  override prepareDate(date: string): string {
    return date.slice(3, 11)
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.eventpreview')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.eventdatum')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.eventname')
  }

  override getUrl(element: Element): string | undefined {
    const path = getAttributeText(element, 'a', 'data-url')
    return new URL(path, this.BASE_URL).toString()
  }
}

register(new Gaskessel())
