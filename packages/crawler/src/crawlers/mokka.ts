import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Mokka extends JSDOMBasedCrawler {
  key = 'mokka'
  public title = 'Mokka'
  public url = 'https://mokka.ch'
  public city = 'Thun'
  dateFormat = 'dd. MMM'

  override getEventElements(page: Document) {
    return getElements(page, 'a.shows')
  }

  override getTitle(element: Element) {
    return getText(element, '.title-section')
  }

  override getStart(element: Element) {
    return getText(element, '.date')
  }

  override getUrl(element: Element) {
    return element.getAttribute('href') ?? ''
  }

  override prepareDate(date: string) {
    return date.slice(4, 11)
  }
}

register(new Mokka())
