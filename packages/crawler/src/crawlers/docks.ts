import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Docks extends JSDOMBasedCrawler {
  key = 'docks'
  title = 'Docks'
  url = 'https://www.docks.ch/programme/'
  city = 'Lausanne'
  dateFormat = 'dd.MM.yyyy'

  override prepareDate(date: string) {
    return date.slice(0, 10)
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.concerts > a')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.programme-item-date')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.event-item-title.programme-item-title')
  }

  override getUrl(element: Element): string | undefined {
    return element.getAttribute('href') ?? ''
  }
}

register(new Docks())
