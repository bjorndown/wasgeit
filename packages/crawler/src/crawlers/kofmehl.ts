import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Kofmehl extends JSDOMBasedCrawler {
  key = 'kofmehl'
  title = 'Kofmehl'
  url = 'https://kofmehl.net/'
  city = 'Solothurn'
  dateFormat = 'dd.MM'

  override prepareDate(date: string) {
    return date.slice(3, 11)
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.events__link')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.events__title')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.events__date') ?? ''
  }

  override getUrl(element: Element): string | undefined {
    return element.getAttribute('href') ?? ''
  }
}

register(new Kofmehl())
