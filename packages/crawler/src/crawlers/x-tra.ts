import { register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

const BASE_URL = 'https://x-tra.ch/'

class XTra extends JSDOMBasedCrawler {
  key = 'xtra'
  title = 'X-TRA'
  url = new URL('/en/program/concerts/', BASE_URL).toString()
  city = 'Zürich'
  dateFormat = 'dd.MM.yy'

  override getEventElements(page: Document) {
    return getElements(page, 'ul.tile li')
  }

  override getTitle(element: Element) {
    return getText(element, 'div h2')
  }

  override getStart(element: Element) {
    return getText(element, 'div h3')
  }

  override getUrl(element: Element) {
    return new URL(getAttributeText(element, 'a', 'href'), BASE_URL).toString()
  }

  override prepareDate(date: string) {
    return date.slice(4)
  }
}

register(new XTra())
