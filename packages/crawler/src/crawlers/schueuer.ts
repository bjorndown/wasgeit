import { register } from '../lib/crawler.ts'
import {
  getAttributeText,
  getElements,
  getText,
  JSDOMBasedCrawler,
} from '../lib/browserless.ts'

class Schueuer extends JSDOMBasedCrawler {
  key = 'schueuer'
  title = 'Schüür'
  url = 'https://www.schuur.ch/programm'
  city = 'Luzern'

  override prepareDate(date: string) {
    return date
  }

  override getEventElements(page: Document) {
    return getElements(page, '.viz-event-list-box')
  }

  override getStart(element: Element) {
    return getAttributeText(element, 'meta[itemprop="startDate"]', 'content')
  }

  override getTitle(element: Element) {
    return getText(element, '.viz-event-name')
  }

  override getUrl(element: Element) {
    return getAttributeText(element, '.viz-event-box-details-link', 'href')
  }
}

register(new Schueuer())
