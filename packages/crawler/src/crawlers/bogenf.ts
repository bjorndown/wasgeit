import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class BogenF extends JSDOMBasedCrawler {
  key = 'bogenf'
  BASE_URL = 'https://www.bogenf.ch'
  title = 'Bogen F'
  city = 'Zürich'
  url = new URL('/konzerte', this.BASE_URL).toString()
  dateFormat = 'dd.MM.yyyy'

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.upcoming-list .upcoming-list-item')
  }

  override getStart(element: Element): string | undefined {
    const timeText = getText(element, 'time')

    return timeText?.split(', ')?.[1]?.split(' | ')[0]
  }

  override getTitle(element: Element): string | undefined {
    const band = getText(element, '.band-headline')
    const support = getText(element, '.band-support')
    return [band, support].filter(value => !!value).join(', ')
  }

  override getUrl(element: Element): string | undefined {
    const link = element.querySelector('a')
    const path = link?.getAttribute('href')
    return new URL(path ?? '', this.BASE_URL).toString()
  }
}

register(new BogenF())
