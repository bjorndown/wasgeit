import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Isc extends JSDOMBasedCrawler {
  key = 'isc'
  title = 'ISC'
  url = 'https://www.isc-club.ch'
  city = 'Bern'
  dateFormat = 'dd.MM.'

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.event_preview')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.event_title_date')
  }

  override getTitle(element: Element): string | undefined {
    return getText(element, '.event_title_title')
  }

  override getUrl(element: Element): string | undefined {
    return element.getAttribute('href') ?? ''
  }
}

register(new Isc())
