import { register } from '../lib/crawler.ts'
import { getElements, getText, JSDOMBasedCrawler } from '../lib/browserless.ts'

class Kiff extends JSDOMBasedCrawler {
  key = 'kiff'
  BASE_URL = 'https://www.kiff.ch'
  title = 'Kiff'
  url = new URL('/de/home.html?view=list', this.BASE_URL).toString()
  city = 'Aarau'
  dateFormat = 'dd MMM'

  override prepareDate(date: string) {
    return date.replace('\n', '').trim().slice(3, 9)
  }

  override getEventElements(page: Document): Element[] {
    return getElements(page, '.programm-grid.listview a')
  }

  override getStart(element: Element): string | undefined {
    return getText(element, '.event-date')
  }

  override getTitle(element: Element): string | undefined {
    return element.getAttribute('title') ?? ''
  }

  override getUrl(element: Element): string | undefined {
    return new URL(element.getAttribute('href') ?? '', this.BASE_URL).toString()
  }
}

register(new Kiff())
