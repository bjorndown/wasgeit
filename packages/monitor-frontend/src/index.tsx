/* @refresh reload */
import { render } from 'solid-js/web'
import './root.css'
import { Component, ErrorBoundary, Suspense } from 'solid-js'
import { Route, Router } from '@solidjs/router'
import Home from '~/pages/Home'
import Run from '~/pages/Run'

const root = document.getElementById('root')

if (import.meta.env.DEV && !(root instanceof HTMLElement)) {
  throw new Error(
    'Root element not found. Did you forget to add it to your index.html? Or maybe the id attribute got misspelled?',
  )
}

const Layout: Component = () => {
  return (
    <>
      <main>
        <Suspense>
          <ErrorBoundary fallback={err => <div>Error: {err.message}</div>}>
            <Router>
              <Route path="/" component={Home} />
              <Route path="/runs/:runKey" component={Run} />
            </Router>
          </ErrorBoundary>
        </Suspense>
      </main>
    </>
  )
}

render(() => <Layout />, root!)
