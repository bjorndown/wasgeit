import Link from 'next/link'

const Impressum = () => {
  return (
    <article>
      <p>
        <Link className="back" href="/">
          Zurück
        </Link>
      </p>
      <h2>Kontakt</h2>
      <p>
        <a href="mailto:hallo@wasgeit.ch">hallo@wasgeit.ch</a>
      </p>
      <h2>Source</h2>
      <p>
        <a href="https://codeberg.org/bjorndown/wasgeit">
          codeberg.org/bjorndown/wasgeit
        </a>
      </p>

      <style jsx>{`
        article {
          padding: 0.5rem;
        }
      `}</style>
    </article>
  )
}

export default Impressum
